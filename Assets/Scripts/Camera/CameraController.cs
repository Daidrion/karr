﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour
{
    private GameObject _player;       
    private Vector3 _cameraOffset;
    private Rigidbody _playerRigigdBody;
    private float _aspectRatio;
    private float _playerSpeed;
    private float _heightModifier;
    [SerializeField]
    private float _minZoomOut, _maxZoomOut, _mouseOffset, _speedOffset;

    void Start()
    {
        _player = GameObject.FindGameObjectWithTag("Player");
        _playerRigigdBody = _player.GetComponent<Rigidbody>();
        _cameraOffset = transform.position - _player.transform.position;
        _heightModifier = _playerSpeed / _speedOffset;
        _aspectRatio = Camera.main.aspect;
    }

    void Update()
    {
        Vector3 newCamPosition = new Vector3();
        _playerSpeed = _playerRigigdBody.velocity.magnitude;
        newCamPosition = _player.transform.position + _cameraOffset;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        newCamPosition.x += ray.direction.x * _mouseOffset/_aspectRatio;
        newCamPosition.z += ray.direction.z * _mouseOffset;
        _heightModifier = Mathf.Lerp(_heightModifier, (_playerSpeed / _speedOffset), 20 * Time.deltaTime);
        newCamPosition.y = Mathf.Lerp(_minZoomOut, _maxZoomOut, _heightModifier);
        // Camera X-axis angle compensation
        newCamPosition.z += -newCamPosition.y / 2.75f;
        transform.position = newCamPosition;
    }
}