﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Turret : MonoBehaviour
{
    private GameObject _turret;

    private Weapon _weapon;
    public Weapon Weapon { get { return _weapon; } set { _weapon = value; } }

    private Vector3 _targetPosition;
    public Vector3 TargetPoisition { get { return _targetPosition; } set { _targetPosition = value; } }

    [SerializeField]
    private float _counterClockWiseY, _clockWiseRightY, _counterClockWiseX, _clockWiseX; 

    public float RotationSpeed;

    void Start()
    {
        _turret = gameObject; 
    }

    void LateUpdate()
    {
        if (_targetPosition != Vector3.zero)
        {
            FollowTarget();
        }
    }

    public void FireWeapon()
    {

    }

    public void FollowTarget()
    {
        // Reminder: InverseTransformDirection global -> local
        Vector3 position = transform.InverseTransformDirection(_targetPosition - transform.position); 
        Quaternion rotation = Quaternion.LookRotation(position);
        rotation = Quaternion.Euler(new Vector3
            (
                ClampAngle(rotation.eulerAngles.x, _clockWiseX, _counterClockWiseX),
                ClampAngle(rotation.eulerAngles.y, _clockWiseRightY, _counterClockWiseY),
                transform.position.z
            ));
        transform.localRotation = Quaternion.Slerp(transform.localRotation, rotation, RotationSpeed * Time.deltaTime);
    }

    // The min rotation would be clock-wise rotation: [0, value], the max rotation is the opposite: [value, 360]
    float ClampAngle(float angle, float minRotation, float maxRotation)
    {
        if (angle < 180)
        {
            angle = Mathf.Clamp(angle, 0, minRotation);
        }
        else
        {
            angle = Mathf.Clamp(angle, maxRotation, 360);
        }
        return angle;
    }
}
