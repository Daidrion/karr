﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum WeaponType
{
    Projectile
}

public enum WeaponSize
{
    Small, 
    Medium,
    Large
}

public abstract class Weapon : MonoBehaviour 
{
    private WeaponType _weaponType;
    private WeaponSize _weaponSize;

    private int ammoMaxCapacity;
    private int ammoClipCapacity;
    private int ammoCurrent;

    private Projectile projectile;

    public abstract void Fire();

    public virtual void Reload()
    {

    }
}
