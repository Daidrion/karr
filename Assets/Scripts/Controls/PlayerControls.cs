﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControls : MonoBehaviour 
{
    private IPlayerControl _controlScheme;
	
    void Start()
    {
        SetControlScheme();
        GameManager.Instance.OnGameChanged += SetControlScheme;
    }

	void Update () 
	{
        _controlScheme.Control();
    }

    void OnDisable()
    {
        if (GameManager.Instance != null) GameManager.Instance.OnGameChanged -= SetControlScheme;
    }

    void SetControlScheme()
    {
        switch(GameManager.Instance.GameState)
        {
            case GameState.Driving:
                _controlScheme = PlayerControlDriving.Instance;   
                break;
            default:
                _controlScheme = PlayerControlStub.Instance;
                break;
        }
    }
}

public interface IPlayerControl
{
    void Control();
}

public class PlayerControlDriving : IPlayerControl
{
    private GameObject _playerObject;
    private Unit _playerUnit;
    private CarController _carController;
    private Vector3 _targetPosition;
    private static PlayerControlDriving _instance;

    public static PlayerControlDriving Instance
    {
        get
        {
            if (_instance == null) _instance = new PlayerControlDriving();
            return _instance;
        }
    }

    private PlayerControlDriving()
    {
        _playerObject = GameObject.FindWithTag("Player");
        _carController = _playerObject.GetComponent<CarController>();
        _playerUnit = _playerObject.GetComponent<Unit>();
    }

    public void Control()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        _carController.Move(Input.GetAxis("Vertical"), Input.GetAxis("Horizontal"));
        if (_playerUnit.Turrets.Length > 0 && Physics.Raycast(ray, out hit, 1000))
        {
            foreach (Turret turret in _playerUnit.Turrets)
            {
                turret.TargetPoisition = hit.point;
            }
        }
    }
}

public class PlayerControlStub : IPlayerControl
{
    private static PlayerControlStub _instance;

    public static PlayerControlStub Instance
    {
        get
        {
            if (_instance == null) _instance = new PlayerControlStub();
            return _instance;
        }
    }

    public void Control()
    {
        Debug.LogWarning(string.Format("The control scheme of GameState.{0} is not implemented!", GameManager.Instance.GameState));
    }
}