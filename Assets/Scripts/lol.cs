﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class lol : MonoBehaviour 
{
    [SerializeField]
    private AudioSource _audio;
    private GameObject _player;
    private Rigidbody _rigidbody;
    private Unit _unit;

    void Start()
    {
        _audio = GetComponent<AudioSource>();
        _player = GameObject.Find("Player");
        _rigidbody = _player.GetComponent<Rigidbody>();
        _unit = _player.GetComponent<Unit>();
    }

    void Update()
    {
        _audio.pitch = Mathf.Lerp(0.5f, 1f, (_rigidbody.velocity.magnitude * 3.5f * Time.deltaTime));
        //Debug.Log(_audio.pitch);
        if (Input.GetKeyDown("t"))
        {
            _unit._carController.maxMotorTorque = 40000f;
            _rigidbody.mass = 250f;
            _rigidbody.drag = 0;
        }
        Debug.Log(_rigidbody.velocity.magnitude);
    }
}
