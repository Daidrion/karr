﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntityList 
{
    public List<GameObject> Entities { get; private set; }

    private static EntityList _instance;
    public static EntityList Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = new EntityList();
            }
            return _instance;
        }
    }
   
    private EntityList()
    {
        Entities = new List<GameObject>();
        Entities.Add(GameObject.Find("Player"));
    }

    public void AddNewEntity(GameObject entity)
    {
        Entities.Add(entity);
    }

    public void RemoveEntity(GameObject entity)
    {
        if (Entities.Contains(entity)) Entities.Remove(entity);
    }
}
