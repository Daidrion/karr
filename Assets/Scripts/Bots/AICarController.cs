﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AICarController : MonoBehaviour 
{
    private CarController _carController;
    //private Rigidbody _rigidbody;
    private Vector3 _pathWaypoint;
    private float _distanceSpeedDamper;

    void OnEnable()
    {
        _pathWaypoint = Vector3.zero;
    }

    void Start()
    {
        _carController = GetComponent<CarController>();
        //_rigidbody = GetComponent<Rigidbody>();
    }

    void Move(float[] inputAxis)
    {
        _carController.Move(inputAxis[0], inputAxis[1]);
    }

    bool TryMakePath(Vector3 destination)
    {
        NavMeshPath path = new NavMeshPath();
        if (NavMesh.CalculatePath(transform.position, destination, NavMesh.AllAreas, path))
        {
            _pathWaypoint = path.corners[1];
            return true;
        }
        else return false;
    }

    public bool TryApproachTarget(Vector3 targetPosition)
    {
        if (TryMakePath(targetPosition))
        {
            float[] aiInputAxis = new float[2];
            Vector3 moveDirection = (_pathWaypoint - transform.position).normalized;
            float moveRotation = Vector3.Angle(transform.forward, moveDirection);

            if (moveRotation < 10f) moveRotation = 0;

            if (moveDirection.z <= -0.2f && moveRotation > 60f)
            {
                aiInputAxis[0] = moveDirection.z;
                if ((transform.InverseTransformPoint(_pathWaypoint).normalized.x) >= 0)
                {
                    aiInputAxis[1] = -((moveRotation / 180) * 2);
                }
                else
                {
                    aiInputAxis[1] = (moveRotation / 180) * 2;
                }
            }
            else
            {
                aiInputAxis[0] = Mathf.Abs(moveDirection.z);
                if ((transform.InverseTransformPoint(_pathWaypoint).normalized.x) >= 0)
                {
                    aiInputAxis[1] = (moveRotation / 180) * 2;
                }
                else
                {
                    aiInputAxis[1] = -((moveRotation / 180) * 2);
                }
            }
            Move(aiInputAxis);
            return true;
        }
        else
        {
            return false;
        }
    }

}
