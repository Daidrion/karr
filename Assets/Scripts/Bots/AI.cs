﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class AI : MonoBehaviour 
{
    private FSM _fsm;
    public Unit Unit { get; private set; }
    public AICarController CarController { get; private set; }
    public Dictionary<Factions, int> FactionStandingsLocal { get; private set; }
    public float AwarenessRadius { get; private set; }
    public int HostileStanding { get; private set; }

    void Start()
    {
        Unit = GetComponent<Unit>();
        CarController = GetComponent<AICarController>();
        FactionStandingsLocal = FactionManager.Instance.GetStandings(Unit.FactionName);
        AwarenessRadius = 100f;
        HostileStanding = -100;
        if (_fsm == null) _fsm = new FSM(Unit, this);
    }

    void Update()
    {
        if (_fsm != null && Unit.IsAlive)
        {
            _fsm.ProcessState();
        }
    }

    public void UpdateLocalStandings()
    {
        FactionStandingsLocal = FactionManager.Instance.GetStandings(Unit.FactionName);
    }

    public void ResetAI()
    {
        if (_fsm != null) _fsm = new FSM(Unit, this);
        UpdateLocalStandings();
    }
}

public class FSM
{
    private FSMState _currentState;
    private AI _ai;
    private List<FSMState> _states = new List<FSMState>();
    private States _transitionState;

    public FSM(Unit unit, AI ai)
    {
        _ai = ai;
        CreateStates();
        _currentState = _states[0];
    }

    public void ProcessState()
    {
        if (_states.Count == 0)
        {
            throw new Exception("No states in FSM!");
        }
        _transitionState = _currentState.CheckForTransition();
        if (_currentState.State != _transitionState)
        {
            _currentState.OnStateExit();
            foreach (FSMState state in _states)
            {
                if (state.State == _transitionState)
                {
                    _currentState = state;
                    break;
                }
                Debug.LogWarning(string.Format("The transition state {0} doesn't exist in _states!", _transitionState));
                return;
            }
            _currentState.OnStateEnter();
        }
        _currentState.Execute();
    }

    public void CreateStates()
    {
        _states.Add(new StateDefault(States.Default, _ai));
        _states.Add(new StateTest(States.Test, _ai));
    }
}

public enum States
{
    Default = 0,
    Test = 1
}

public abstract class FSMState
{
    protected AI _ai;
    public States State { get; private set; }

    public FSMState(States state, AI ai)
    {
        _ai = ai;
        State = state;
    }

    public abstract States CheckForTransition();
    public abstract void Execute();
    public virtual void OnStateEnter() { }
    public virtual void OnStateExit() { }

    public virtual GameObject SearchTarget(bool isHostile)
    {
        GameObject targetObject = null;
        Unit unit;
        Factions targetFaction;

        Collider[] colliders = Physics.OverlapSphere(_ai.Unit.gameObject.transform.position, _ai.AwarenessRadius, 1 << 8);
        var sortedTargets = colliders.OrderBy(t => (t.transform.position - _ai.Unit.transform.position).sqrMagnitude);
        //var sortedUnits = colliders.OrderBy(t => Helper.GetPathLength(t.transform.position, _ai.Unit.transform.position));

        foreach (Collider target in sortedTargets)
        {
            if (target.transform != _ai.Unit.gameObject.transform)
            {
                unit = target.GetComponent<Unit>();
                targetFaction = unit.FactionName;

                if (isHostile)
                {
                    if (targetFaction == _ai.Unit.FactionName) continue;
                    if (_ai.FactionStandingsLocal[targetFaction] <= _ai.HostileStanding)
                    {
                        targetObject = target.gameObject;
                        break;
                    }
                }
                else
                {
                    if (targetFaction == _ai.Unit.FactionName)
                    {
                        targetObject = target.gameObject;
                        break;
                    }
                }
            }
        }
        return targetObject;
    }
}

public class StateDefault : FSMState
{
    private GameObject _target;

    public StateDefault(States state, AI ai) : base(state, ai)
    {
    }

    public override States CheckForTransition()
    {
        return States.Default;
    }

    public override void Execute()
    {
        _target = SearchTarget(true);

        if (_target == null) return;

        _ai.CarController.TryApproachTarget(_target.transform.position);
        if (_ai.Unit.Turrets.Length > 0)
        {
            foreach (Turret turret in _ai.Unit.Turrets)
            {
                turret.TargetPoisition = _target.transform.position;
                turret.FireWeapon();
            }
        }
        
    }
}

public class StateTest : FSMState
{ 
    public StateTest(States state, AI ai) : base(state, ai)
    {
    }

    public override States CheckForTransition()
    {
        return States.Test;
    }

    public override void Execute()
    {
        Debug.Log(this.GetType().Name);
    }
}