﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public static class Helper  
{
    public static float GetPathLength(Vector3 position1, Vector3 position2)
    {
        float length = Mathf.Infinity;
        NavMeshPath path = new NavMeshPath();
        NavMesh.CalculatePath(position1, position2, NavMesh.AllAreas, path);
        //if (path != )
        return path.corners.Length;
    }
}
