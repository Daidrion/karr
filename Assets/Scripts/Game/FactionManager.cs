﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Factions
{
    Default,
    Player,
    Bandits,
    Neutral
}

public class FactionManager
{
    private Dictionary<Factions, Faction> _factions;

    private static FactionManager _instance;
    public static FactionManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = new FactionManager();
            }
            return _instance;
        }
    }

    private FactionManager()
    {
        DefaultStandings standings = new DefaultStandings();
        _factions = new Dictionary<Factions, Faction>();
        foreach (Factions faction in System.Enum.GetValues(typeof(Factions)))
        {
            if (faction == 0) continue;
            _factions.Add(faction, new Faction(faction, standings));
        }
    }

    public void ChangeFactionStanding(Factions changer, Factions reciever, int delta)
    {
        Faction faction;
        _factions.TryGetValue(reciever, out faction);
        faction.ChangeStanding(changer, delta);
    }

    public Dictionary<Factions, int> GetStandings(Factions factionName)
    {
        return _factions[factionName].Standings;
    }
}

public class Faction
{
    public Factions Name { get; private set; }
    public Dictionary<Factions, int> Standings { get; private set; }

    public Faction(Factions name, DefaultStandings defaultStandings)
    {
        Standings = new Dictionary<Factions, int>();
        Name = name;
        Standings = defaultStandings.GetStandings(name);
    }

    public void ChangeStanding(Factions changer, int standingDelta)
    {
        Standings[changer] += standingDelta;
    }
}

// Lol
public class DefaultStandings
{
    public Dictionary<Factions, NestedStandings> StandingsAll { get; private set; }

    public DefaultStandings()
    {
        StandingsAll = new Dictionary<Factions, NestedStandings>();

        var standings = new NestedStandings();
        standings.SetStandings(Factions.Player, -100);
        standings.SetStandings(Factions.Neutral, -100);
        StandingsAll.Add(Factions.Bandits, standings);

        standings = new NestedStandings();
        standings.SetStandings(Factions.Player, 50);
        standings.SetStandings(Factions.Bandits, -100);
        StandingsAll.Add(Factions.Neutral, standings);

        standings = new NestedStandings();
        standings.SetStandings(Factions.Bandits, -100);
        standings.SetStandings(Factions.Neutral, 50);
        StandingsAll.Add(Factions.Player, standings);
    }

    public Dictionary<Factions, int> GetStandings(Factions factionName)
    {
        NestedStandings standings;
        StandingsAll.TryGetValue(factionName, out standings);
        return standings.ConcreteFactionStandings;
    }

    // Such readbility, much amaze
    public class NestedStandings
    {
        public Dictionary<Factions, int> ConcreteFactionStandings { get; private set; }
        public NestedStandings()
        {
            ConcreteFactionStandings = new Dictionary<Factions, int>();
        }

        public void SetStandings(Factions faction, int value)
        {
            ConcreteFactionStandings.Add(faction, value);
        }
    }
} 