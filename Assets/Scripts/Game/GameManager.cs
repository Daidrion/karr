﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GameState
{
    Driving,
    Paused,
    Interface
}

public class GameManager : MonoBehaviour
{
    private static bool _isBeingDestroyed;
    public delegate void GameStateChange();
    public event GameStateChange OnGameChanged;

    private FactionManager _factionManager;

    public GameState GameState { get; private set; }

    private static GameManager _instance;
    public static GameManager Instance
    {
        get
        {
            if (_isBeingDestroyed) return null;
            if (_instance == null)
            {
                GameObject go = new GameObject("GameManager");
                go.AddComponent<GameManager>();
            }
            return _instance;
        }
    }

    void Awake()
    {
        _instance = this;
        _factionManager = FactionManager.Instance;
        ChangeGameState(GameState.Driving);
    }

    void OnDestroy()
    {
        _isBeingDestroyed = true;
    }

    public void ChangeGameState(GameState state)
    {
        GameState = state;
        if (OnGameChanged != null) OnGameChanged();
    }
}
