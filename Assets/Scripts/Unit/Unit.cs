﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Unit : MonoBehaviour
{
    [SerializeField]
    public CarController _carController;
    [SerializeField]
    private Transform _centerOfMass;

    public float maxMotorTorque;
    public float maxSteeringAngle;

    public Factions FactionName { get; private set; }

    private Rigidbody _carRigidbody;

    public Turret[] Turrets; // ToDo: incapsulate
    private Weapon[] _weapons;

    public bool IsAlive { get; private set; }

    void OnEnable()
    {
        switch (gameObject.tag)
        {
            case "Player":
                {
                    FactionName = Factions.Player;
                    break;
                }
            case "Faction1":
                {
                    FactionName = Factions.Bandits;
                    break;
                }
        }
    }

    void Start () 
	{
        IsAlive = true;
        if (_carController == null) _carController = GetComponent<CarController>();
        _carController.maxMotorTorque = maxMotorTorque;
        _carController.maxSteeringAngle = maxSteeringAngle;
        _carRigidbody = GetComponent<Rigidbody>();
        //carRigidbody.centerOfMass = _centerOfMass.position;
	}
}